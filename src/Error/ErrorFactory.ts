import Boom from '@hapi/boom';
import { HTTP_STATUS } from '../helpers/Enums';

type STATUS = number;

export const ErrorFactory = (message: any, status: STATUS): void => {
  switch (status) {
    case HTTP_STATUS.BAD_REQUEST:
      throw Boom.badRequest(message);
    case HTTP_STATUS.UNAUTHORIZED:
      throw Boom.unauthorized(message);
    case HTTP_STATUS.FORBIDDEN:
      throw Boom.forbidden(message);
    case HTTP_STATUS.NOT_FOUND:
      throw Boom.notFound(message);
    default:
      throw new Error('Generic Error');
  }
};
