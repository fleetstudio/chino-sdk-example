import { ChinoBucket, ChinoEndpoint, ChinoGroup, ChinoPermissionStrategy, ChinoSessionDTO } from "@phinxlab/chino-sdk";

export class TestPermissionStrategy implements ChinoPermissionStrategy {
  name: string

  constructor() {
    this.name = 'BASE';
  }

  async checkPermission(session: ChinoSessionDTO, service: ChinoBucket | ChinoEndpoint<any>): Promise<void> {
    let type = service instanceof ChinoBucket ? 'Bucket' : 'Endpoint';
    if (service.groups === undefined || session.user.groups === undefined) throw new Error(`You need to define the group access to the ${type} and to the User when allowGues=false`);
    const found: ChinoGroup | undefined = service.groups.find((group: ChinoGroup) => (session.user.groups.find((allowed: ChinoGroup) => allowed.name.toLocaleLowerCase() === group.name.toLocaleLowerCase())));
    if (!found) throw new Error(`No applicable permission for "${service.name}" on user "${session.user.name}"`);
  }
}