import {
  ChinoEncryptManager,
  ChinoGroup,
  ChinoUser,
  ChinoUserFactory,
  ChinoValidationMethod,
} from '@phinxlab/chino-sdk';
import { Account } from '../../generated/orm/entities';
import { AccountDAO } from '../../generated/orm/dao';
import { JRUser, JRCredentials } from "@phinxlab/just-rpc";

class TestUser extends ChinoUser {
  constructor(user: Account) {
    super();
    this.id = user.accountId.toString();
    this.username = user.username!;
    this.password = user.password!;
    this.nick = user.nick!;
    this.groups = [{ id: user.role.groupId.toString(), name: user.role.name! }];
    // depends to database or overwrite groups
  }
}

export class TestUserFactory extends ChinoUserFactory {
  async findByID(id: string): Promise<ChinoUser> {
    const [account] = await AccountDAO.query().equals('user_id', id).run();

    if (!account) throw new Error('Account is not exists');

    return new TestUser(account);
  }

  async findByNick(nick: string): Promise<ChinoUser> {
    const [account] = await AccountDAO.query().equals('nick', nick).run();

    if (!account) throw new Error('Account is not exists');

    return new TestUser(account);
  }

  async findByUsername(username: string): Promise<ChinoUser> {
    const [account] = await AccountDAO.query()
      .equals('username', username)
      .run();

    if (!account) throw new Error('Account is not exists');

    return new TestUser(account);
  }

  async authenticate(credentials: JRCredentials): Promise<JRUser> {
    const user: JRUser = await this.findUser(credentials)
    const decryptPassword = await ChinoEncryptManager.decrypt(user.password)

    if (credentials.password !== decryptPassword) {
      throw new Error(`User or password incorrect`);
    }
    return user;
  }

  findByEmail(email: string): Promise<ChinoUser> {
    throw new Error('Method not implemented.');
  }

  findByGroupID(id: string): Promise<ChinoGroup> {
    throw new Error('Method not implemented.');
  }

  async findByGroupName(name: string): Promise<ChinoGroup> {
    throw new Error('Methods not implemented');
  }

  getValidationMethod(): ChinoValidationMethod {
    return ChinoValidationMethod.USERNAME;
  }
}
