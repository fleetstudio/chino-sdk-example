import { ChinoAutoLoaderBucket, ChinoGroup, Config } from '@phinxlab/chino-sdk';

class Bucket extends ChinoAutoLoaderBucket {
  constructor(
    endspointPath: string,
    name: string,
    path: string,
    groups: Array<ChinoGroup> = [],
  ) {
    super(name, path);
    this.configure();
    this.read(Config.fromBBD(endspointPath));
    this.addGroup(groups);
  }
}

export class PublicBucket extends Bucket {
  configure() {
    this.allowGuest = true;
  }
}

export class PrivateBucket extends Bucket {
  configure() {
    this.allowGuest = false;
  }
}
