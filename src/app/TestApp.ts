import {
  ChinoApp,
  ChinoBucket,
  ChinoEvent,
  Config,
  TraverseAutoloader,
} from '@phinxlab/chino-sdk';
import { PrivateBucket, PublicBucket } from './Config';
import { AdminGroup, DirectorGroup, UserGroup } from '../helpers/consts/grupos';

export class TestApp implements ChinoApp {
  event(event: ChinoEvent, name: string, data: any): void { }

  getBuckets(): ChinoBucket[] {
    const sessionBucket = new PublicBucket(
      'generated/endpoints/chino',
      'Session Bucket',
      '/session',
    );

    // Charge groups to Bucket
    const apiBucket = new PrivateBucket(
      'generated/endpoints/sdk',
      'Api Bucket',
      '/api',
      [AdminGroup, UserGroup, DirectorGroup],
    );

    return [sessionBucket, apiBucket];
  }

  configureTraverses(): void {
    TraverseAutoloader.create(Config.fromBBD('app/orm/traverse'));
  }
  configureValidators(): void { }
}
