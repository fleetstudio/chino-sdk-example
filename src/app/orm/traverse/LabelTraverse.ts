/* eslint-disable @typescript-eslint/ban-types */
import {
  ChinoContext,
  LQLQueryFactory,
  ObjectType,
  TraverseEntityEvent,
  TraverseEntity,
} from '@phinxlab/chino-sdk';
import { Label, Account } from '../../../generated/orm/entities';

export class NotebookTraverse extends TraverseEntity<Label> {
  constructor() {
    super(Label, 0);
  }

  async match(event: TraverseEntityEvent, context: ChinoContext) {
    return (
      event === TraverseEntityEvent.Insert ||
      event === TraverseEntityEvent.Select
    );
  }

  async onInsert(entity: Label, context: ChinoContext, stopFlow: Function) {
    const {
      user: { id },
    } = context.session;
    const account = new Account();
    account.accountId = Number(id);
    entity.account = account;
  }

  async onRemove(
    entity: ObjectType<Label>,
    context: ChinoContext,
    stopFlow: Function,
  ) {}

  async onSelect(
    query: LQLQueryFactory<Label>,
    context: ChinoContext,
    stopFlow: Function,
  ) {
    query.wrap().equals('account_id', context.session.user.id);
  }

  async onUpdate(
    entity: ObjectType<Label>,
    context: ChinoContext,
    stopFlow: Function,
  ) {}
}
