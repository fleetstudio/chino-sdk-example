/* eslint-disable @typescript-eslint/ban-types */
import {
  ChinoContext,
  LQLQueryFactory,
  ObjectType,
  TraverseEntityEvent,
  TraverseEntity,
} from '@phinxlab/chino-sdk';
import { Notebook, Account } from '../../../generated/orm/entities';

export class NotebookTraverse extends TraverseEntity<Notebook> {
  constructor() {
    super(Notebook, 0);
  }

  async match(event: TraverseEntityEvent, context: ChinoContext) {
    return (
      event === TraverseEntityEvent.Insert ||
      event === TraverseEntityEvent.Select
    );
  }

  async onInsert(entity: Notebook, context: ChinoContext, stopFlow: Function) {
    const {
      user: { id },
    } = context.session;
    const account = new Account();
    account.accountId = Number(id);
    entity.account = account;
  }

  async onRemove(
    entity: ObjectType<Notebook>,
    context: ChinoContext,
    stopFlow: Function,
  ) {}

  async onSelect(
    query: LQLQueryFactory<Notebook>,
    context: ChinoContext,
    stopFlow: Function,
  ) {
    query.wrap().equals('account_id', context.session.user.id);
  }

  async onUpdate(
    entity: ObjectType<Notebook>,
    context: ChinoContext,
    stopFlow: Function,
  ) {}
}
