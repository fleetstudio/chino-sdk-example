/* eslint-disable @typescript-eslint/ban-types */
import {
  ChinoContext,
  LQLQueryFactory,
  ObjectType,
  TraverseEntityEvent,
  TraverseEntity,
} from '@phinxlab/chino-sdk';
import { Note, Account } from '../../../generated/orm/entities';

export class NotebookTraverse extends TraverseEntity<Note> {
  constructor() {
    super(Note, 0);
  }

  async match(event: TraverseEntityEvent, context: ChinoContext) {
    return (
      event === TraverseEntityEvent.Insert ||
      event === TraverseEntityEvent.Select
    );
  }

  async onInsert(entity: Note, context: ChinoContext, stopFlow: Function) {
    const {
      user: { id },
    } = context.session;
    const account = new Account();
    account.accountId = Number(id);

    entity.account = account;
  }

  async onRemove(
    entity: ObjectType<Note>,
    context: ChinoContext,
    stopFlow: Function,
  ) {}

  async onSelect(
    query: LQLQueryFactory<Note>,
    context: ChinoContext,
    stopFlow: Function,
  ) {
    query.wrap().equals('account_id', context.session.user.id);
  }

  async onUpdate(
    entity: ObjectType<Note>,
    context: ChinoContext,
    stopFlow: Function,
  ) {}
}
