/* eslint-disable @typescript-eslint/ban-types */
import {
  ChinoContext,
  LQLQueryFactory,
  ObjectType,
  TraverseEntityEvent,
  TraverseEntity,
} from '@phinxlab/chino-sdk';
import { NoteLabel, Account } from '../../../generated/orm/entities';

export class NotebookTraverse extends TraverseEntity<NoteLabel> {
  constructor() {
    super(NoteLabel, 0);
  }

  async match(event: TraverseEntityEvent, context: ChinoContext) {
    return (
      event === TraverseEntityEvent.Insert ||
      event === TraverseEntityEvent.Select
    );
  }

  async onInsert(entity: NoteLabel, context: ChinoContext, stopFlow: Function) {
    const {
      user: { id },
    } = context.session;
    const account = new Account();
    account.accountId = Number(id);

    entity.note.account = account;
    entity.label.account = account;
  }

  async onRemove(
    entity: ObjectType<NoteLabel>,
    context: ChinoContext,
    stopFlow: Function,
  ) {}

  async onSelect(
    query: LQLQueryFactory<NoteLabel>,
    context: ChinoContext,
    stopFlow: Function,
  ) {}

  async onUpdate(
    entity: ObjectType<NoteLabel>,
    context: ChinoContext,
    stopFlow: Function,
  ) {}
}
