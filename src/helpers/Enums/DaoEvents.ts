export enum DaoEvents {
  REMOVE = 'Remove',
  INSERT = 'Insert',
  UPDATE = 'Update',
  SELECT = 'Select',
}
