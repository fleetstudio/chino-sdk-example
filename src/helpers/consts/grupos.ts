import { ChinoGroup } from "@phinxlab/chino-sdk";

export const PublicGroup: ChinoGroup = { id: "-1", name: "guest" };
export const AdminGroup: ChinoGroup = { id: "1", name: "admin" };
export const UserGroup: ChinoGroup = { id: "2", name: "user" };
export const DirectorGroup: ChinoGroup = { id: "3", name: "director" };