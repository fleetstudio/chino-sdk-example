import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Account } from './Account';

@Index('groups_pkey', ['groupId'], { unique: true })
@Entity('groups', { schema: 'chino' })
export class Groups {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'group_id' })
  groupId: number;

  @Column('text', { name: 'name', nullable: true })
  name: string | null;
}
