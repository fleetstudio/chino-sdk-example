import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Note } from './Note';
import { Account } from './Account';

@Index('notebook_pkey', ['notebookId'], { unique: true })
@Entity('notebook', { schema: 'sdk' })
export class Notebook {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'notebook_id' })
  notebookId: number;

  @Column('text', { name: 'name', nullable: true })
  name: string | null;

  @OneToMany(() => Note, (note) => note.notebook)
  notes: Note[];

  @ManyToOne(() => Account)
  @JoinColumn([{ name: 'account_id', referencedColumnName: 'accountId' }])
  account: Account;
}
