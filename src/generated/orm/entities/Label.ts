import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Account } from './Account';
import { NoteLabel } from './NoteLabel';

@Index('label_pkey', ['labelId'], { unique: true })
@Entity('label', { schema: 'sdk' })
export class Label {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'label_id' })
  labelId: number;

  @Column('text', { name: 'name', nullable: true })
  name: string | null;

  @ManyToOne(() => Account)
  @JoinColumn([{ name: 'account_id', referencedColumnName: 'accountId' }])
  account: Account;

  @OneToMany(() => NoteLabel, (noteLabel) => noteLabel.label)
  noteLabels: NoteLabel[];
}
