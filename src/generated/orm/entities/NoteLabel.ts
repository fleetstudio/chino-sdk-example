import {
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Label } from './Label';
import { Note } from './Note';

@Index('note_label_pkey', ['noteLabelId'], { unique: true })
@Entity('note_label', { schema: 'sdk' })
export class NoteLabel {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'note_label_id' })
  noteLabelId: number;
  //TODO: tube que agregar el cascade, para que pudiera insertar
  @ManyToOne(() => Label, (label) => label.noteLabels, { cascade: true })
  @JoinColumn([{ name: 'label_id', referencedColumnName: 'labelId' }])
  label: Label;

  @ManyToOne(() => Note, (note) => note.noteLabels, { cascade: true })
  @JoinColumn([{ name: 'note_id', referencedColumnName: 'noteId' }])
  note: Note;
}
