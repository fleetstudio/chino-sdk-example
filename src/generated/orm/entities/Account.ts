import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Groups } from './Groups';
import { Label } from './Label';
import { Note } from './Note';
import { Notebook } from './Notebook';

@Index('account_pkey', ['accountId'], { unique: true })
@Entity('account', { schema: 'chino' })
export class Account {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'account_id' })
  accountId: number;

  @Column('text', { name: 'username', nullable: true })
  username: string | null;

  @Column('text', { name: 'password', nullable: true })
  password?: string | null;

  @Column('text', { name: 'nick', nullable: true })
  nick: string | null;

  @ManyToOne(() => Groups)
  @JoinColumn([{ name: 'role_id', referencedColumnName: 'groupId' }])
  role: Groups;
}
