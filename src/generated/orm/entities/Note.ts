import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Account } from './Account';
import { Notebook } from './Notebook';
import { NoteLabel } from './NoteLabel';

@Index('note_pkey', ['noteId'], { unique: true })
@Entity('note', { schema: 'sdk' })
export class Note {
  //TODO: verificar cuando la primary key es creada a base de secuencias
  /*   @Column('integer', { primary: true, name: 'note_id' })
  noteId: number; */
  @PrimaryGeneratedColumn({ type: 'integer', name: 'note_id' })
  noteId: number;

  @Column('text', { name: 'title', nullable: true })
  title: string | null;

  @Column('text', { name: 'content', nullable: true })
  content: string | null;

  @ManyToOne(() => Account)
  @JoinColumn([{ name: 'account_id', referencedColumnName: 'accountId' }])
  account: Account;

  @ManyToOne(() => Notebook, (notebook) => notebook.notes)
  @JoinColumn([{ name: 'notebook_id', referencedColumnName: 'notebookId' }])
  notebook: Notebook;

  @OneToMany(() => NoteLabel, (noteLabel) => noteLabel.note)
  noteLabels: NoteLabel[];
}
