import { ChinoDAO, ChinoDAOEvent, EntityManager } from '@phinxlab/chino-sdk';
import { Account } from '../../entities';

class AccountDAO extends ChinoDAO<Account> {
  constructor() {
    super(Account, 'account');
  }
}
const i: AccountDAO = new AccountDAO();
export { i as AccountDAO };
