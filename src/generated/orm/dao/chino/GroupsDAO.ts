import { ChinoDAO } from '@phinxlab/chino-sdk';
import { Groups } from '../../entities';

class GroupsDAO extends ChinoDAO<Groups> {
  constructor() {
    super(Groups, 'groups');
  }
}
const i: GroupsDAO = new GroupsDAO();
export { i as GroupsDAO };
