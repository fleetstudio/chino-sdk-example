import { ChinoDAO } from '@phinxlab/chino-sdk';
import { Notebook } from '../../entities';

class NotebookDAO extends ChinoDAO<Notebook> {
  constructor() {
    super(Notebook, 'notebook');
  }

  async postAction(event: any, values: any, txEntityManager: any) {
    console.log('Hi i am pos-action-dao-Notebook');
    if (event === 'Select') {
      values.map((value: Notebook) => {
        delete value.account.password;
        value.notes.map((value) => {
          delete value.account.password;
          value.noteLabels.map((value) => {
            delete value.label.account.password;
            return value;
          });
          return value;
        });
        return value;
      });
    }
    return values;
  }
}
const i: NotebookDAO = new NotebookDAO();
export { i as NotebookDAO };
