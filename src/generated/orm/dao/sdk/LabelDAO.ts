import { ChinoDAO } from '@phinxlab/chino-sdk';
import { Label, NoteLabel } from '../../entities';

class LabelDAO extends ChinoDAO<Label> {
  constructor() {
    super(Label, 'label');
  }

  async postAction(event: any, values: any, txEntityManager: any) {
    console.log('Hi i am pos-action-dao-Label');
    if (event === 'Select') {
      values.map((value: Label) => {
        delete value.account.password;
        value.noteLabels.map((value: NoteLabel) => {
          delete value.note.account.password;
          delete value.note.notebook.account.password;
          return value;
        });
        return value;
      });
    }
    return values;
  }
}
const i: LabelDAO = new LabelDAO();
export { i as LabelDAO };
