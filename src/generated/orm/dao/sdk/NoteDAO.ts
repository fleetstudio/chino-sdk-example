import { ChinoDAO } from '@phinxlab/chino-sdk';
import { Note, Notebook } from '../../entities';

class NoteDAO extends ChinoDAO<Note> {
  constructor() {
    super(Note, 'note');
  }

  async preAction(event: any, value: any, txEntityManager: any) {
    console.log('Hi i am pre-action-dao');
    if (event === 'Insert') {
      const notebook: Notebook = await txEntityManager
        .getRepository(Notebook)
        .findOne(value.notebook.notebookId, {
          where: {
            account: { accountId: value.account.accountId },
          },
        });
      if (!notebook) throw new Error('Notebook not found');
    }
    return value;
  }

  async postAction(event: any, values: any, txEntityManager: any) {
    console.log('Hi i am pos-action-dao-Note');
    if (event === 'Select') {
      return values.map((value: Note) => {
        delete value.account.password;
        delete value.notebook.account.password;
        return value;
      });
    }
    return values;
  }
  async validate(value: any) {
    return value;
  }
}
const i: NoteDAO = new NoteDAO();
export { i as NoteDAO };
