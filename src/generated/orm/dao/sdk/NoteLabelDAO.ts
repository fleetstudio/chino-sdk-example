import { ChinoDAO } from '@phinxlab/chino-sdk';
import { NoteLabel, Label, Note } from '../../entities';

class NoteLabelDAO extends ChinoDAO<NoteLabel> {
  constructor() {
    super(NoteLabel, 'notelabel');
  }
  async preAction(event: any, value: any, txEntityManager: any) {
    console.log('Hi i am pre-action-dao-NoteLabel');
    if (event === 'Insert') {
      const note = await txEntityManager
        .getRepository(Note)
        .findOne(value.note.noteId, {
          where: { account: { accountId: value.note.account.accountId } },
        });
      const label = await txEntityManager
        .getRepository(Label)
        .findOne(value.label.labelId, {
          where: { account: { accountId: value.label.account.accountId } },
        });
      const query = this.query()
        .equals('label_id', value.label.labelId)
        .and()
        .equals('note_id', value.note.noteId);
      const exist = await query.run(txEntityManager);
      if (!note || !label) throw new Error('Label or Note not found');
      if (exist.length) throw new Error('The note already has this label');
      delete value.label.account;
      delete value.note.account;
      return value;
    }

    return value;
  }

  async postAction(event: any, values: any, txEntityManager: any) {
    console.log('Hi i am pos-action-dao-NoteLabel');
    if (event === 'Select') {
      return values.map((value: NoteLabel) => {
        delete value.label.account.password;
        delete value.note.account.password;
        delete value.note.notebook.account.password;
        return value;
      });
    }
    return values;
  }
}
const i: NoteLabelDAO = new NoteLabelDAO();
export { i as NoteLabelDAO };
