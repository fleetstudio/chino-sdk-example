import { ChinoEndpoint } from '@phinxlab/chino-sdk';
import { LabelDAO } from '../../orm/dao';
import { Label } from '../../orm/entities';
import { DirectorGroup } from '../../../helpers/consts/grupos';

export class LabelEndpoint extends ChinoEndpoint<Label> {
  constructor() {
    super(
      LabelDAO.entity.toLowerCase(),
      '/sdk/' + LabelDAO.entity.toLowerCase(),
      LabelDAO,
    );
  }

  getAllowGuest(): boolean {
    return false;
  }

  configure(): void {
    this.addGroup(DirectorGroup)
  }
}
