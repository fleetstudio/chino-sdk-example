import { ChinoEndpoint } from '@phinxlab/chino-sdk';
import { NotebookDAO } from '../../orm/dao';
import { Notebook } from '../../orm/entities';
import { DirectorGroup } from '../../../helpers/consts/grupos';

export class NotebookEndpoint extends ChinoEndpoint<Notebook> {
  constructor() {
    super(
      NotebookDAO.entity.toLowerCase(),
      '/sdk/' + NotebookDAO.entity.toLowerCase(),
      NotebookDAO,
    );
  }

  getAllowGuest(): boolean {
    return false;
  }
  configure(): void {
    this.addGroup(DirectorGroup)
  }
}
