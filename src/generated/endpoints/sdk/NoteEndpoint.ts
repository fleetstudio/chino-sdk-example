import { ChinoEndpoint } from '@phinxlab/chino-sdk';
import { NoteDAO } from '../../orm/dao';
import { Note } from '../../orm/entities';
import { AdminGroup } from '../../../helpers/consts/grupos';

export class NoteEndpoint extends ChinoEndpoint<Note> {
  constructor() {
    super(
      NoteDAO.entity.toLowerCase(),
      '/sdk/' + NoteDAO.entity.toLowerCase(),
      NoteDAO,
    );
  }
  async preInsertAction(req: any, session: any): Promise<any> {
    console.log('Hi im pre-Insert-actions-endpoints-Note');
  }

  async preSelectAction(req: any, session: any): Promise<any> {
    console.log('Hi im pre-select-actions-endpoints-Note');
  }

  configure(): void {
    this.addGroup(AdminGroup)
  }

  getAllowGuest(): boolean {
    return false;
  }
}
