import { ChinoEndpoint } from '@phinxlab/chino-sdk';
import { NoteLabelDAO } from '../../orm/dao';
import { NoteLabel } from '../../orm/entities';
import { UserGroup } from '../../../helpers/consts/grupos';

export class NoteLabelEndpoint extends ChinoEndpoint<NoteLabel> {
  constructor() {
    super(
      NoteLabelDAO.entity.toLowerCase(),
      '/sdk/' + NoteLabelDAO.entity.toLowerCase(),
      NoteLabelDAO,
    );
  }

  getAllowGuest(): boolean {
    return false;
  }

  configure(): void {
    this.addGroup(UserGroup)
  }
}
