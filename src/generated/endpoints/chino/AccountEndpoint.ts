import { ChinoEndpoint } from '@phinxlab/chino-sdk';
import { AccountDAO } from '../../orm/dao';
import { Account } from '../../orm/entities';

export class AccountEndpoint extends ChinoEndpoint<Account> {
  constructor() {
    super(
      AccountDAO.entity.toLowerCase(),
      '/chino/' + AccountDAO.entity.toLowerCase(),
      AccountDAO,
    );
  }
}
