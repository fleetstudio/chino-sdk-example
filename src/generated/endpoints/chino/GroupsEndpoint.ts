import { ChinoEndpoint } from '@phinxlab/chino-sdk';
import { GroupsDAO } from '../../orm/dao';
import { Groups } from '../../orm/entities';

export class GroupsEndpoint extends ChinoEndpoint<Groups> {
  constructor() {
    super(
      GroupsDAO.entity.toLowerCase(),
      '/chino/' + GroupsDAO.entity.toLowerCase(),
      GroupsDAO,
    );
  }
}
