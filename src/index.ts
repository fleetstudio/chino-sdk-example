import { ChinoServerConfig } from '@phinxlab/chino-sdk';
import { chino_runner } from '@phinxlab/chino-sdk/build/chino-runner';
import { TestApp } from './app/TestApp';
import { TestPermissionStrategy, TestUserFactory } from './app/business';

const initChino = async () => {
  const chinoConfig: ChinoServerConfig = {
    app: new TestApp(),
    strategy: {
      userFactory: new TestUserFactory(),
      permissionStrategy: new TestPermissionStrategy(),
    },
  };

  await chino_runner(chinoConfig);
};

initChino();
