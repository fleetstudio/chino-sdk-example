CREATE TABLE IF NOT EXISTS "chino"."groups"(
  group_id SERIAL,
  name TEXT,
  PRIMARY KEY("group_id")
);

ALTER TABLE "chino"."account" ADD COLUMN IF NOT EXISTS nick TEXT;

ALTER TABLE "chino"."account" ADD COLUMN IF NOT EXISTS role_id INT NOT NULL;

ALTER TABLE "chino"."account" ADD FOREIGN KEY ("role_id") REFERENCES "chino"."groups"("group_id"); 
