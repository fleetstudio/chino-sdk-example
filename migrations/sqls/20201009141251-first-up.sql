CREATE SCHEMA IF NOT EXISTS sdk;
CREATE SCHEMA IF NOT EXISTS chino;

CREATE TABLE IF NOT EXISTS "chino"."account"(
  account_id SERIAL,
  username TEXT NOT NULL,
  password TEXT NOT NULL,
  PRIMARY KEY("account_id")
);

CREATE TABLE IF NOT EXISTS "sdk"."notebook"(
  notebook_id SERIAL,
  name TEXT,
  account_id INT,
  PRIMARY KEY("notebook_id")
);

ALTER TABLE "sdk"."notebook" ADD FOREIGN KEY ("account_id") REFERENCES "chino"."account"("account_id");

