CREATE TABLE IF NOT EXISTS "sdk"."note"(
  note_id SERIAL,
  title TEXT,
  CONTENT TEXT,
  account_id INT,
  PRIMARY KEY("note_id")
);

ALTER TABLE "sdk"."note" ADD FOREIGN KEY ("account_id") REFERENCES "chino"."account"("account_id");

CREATE TABLE IF NOT EXISTS "sdk"."label"(
  label_id SERIAL,
  name TEXT,
  account_id INT,
  PRIMARY KEY("label_id")
);

ALTER TABLE "sdk"."label" ADD FOREIGN KEY ("account_id") REFERENCES "chino"."account"("account_id");

CREATE TABLE IF NOT EXISTS "sdk"."note_label"(
  note_label_id SERIAL,
  note_id INT NULL,
  label_id INT,
  PRIMARY KEY("note_label_id")
);

ALTER TABLE "sdk"."note_label" ADD FOREIGN KEY ("note_id") REFERENCES "sdk"."note"("note_id");
ALTER TABLE "sdk"."note_label" ADD FOREIGN KEY ("label_id") REFERENCES "sdk"."label"("label_id");
