/* Replace with your SQL commands */
CREATE SEQUENCE IF NOT EXISTS "sdk"."note_note_id_seq";

ALTER TABLE "sdk"."note" ALTER COLUMN "note_id" SET NOT NULL;
ALTER TABLE "sdk"."note" ALTER COLUMN "note_id" SET DEFAULT nextval('sdk.note_note_id_seq'::regclass);
ALTER SEQUENCE "sdk"."note_note_id_seq" OWNED BY "sdk"."note"."note_id";
