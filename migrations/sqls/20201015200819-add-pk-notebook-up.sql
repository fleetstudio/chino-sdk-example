/* Replace with your SQL commands */

ALTER TABLE "sdk"."note" ADD COLUMN "notebook_id" INT;

ALTER TABLE "sdk"."note" ADD FOREIGN KEY ("notebook_id") REFERENCES "sdk"."notebook"("notebook_id");